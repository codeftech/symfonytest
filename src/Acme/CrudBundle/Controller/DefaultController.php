<?php

namespace Acme\CrudBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Acme\CrudBundle\Entity\Student;

class DefaultController extends Controller
{
	/**
     * @Route("/", name="_demo_student")
     * @Template()
	 */
    public function indexAction()
    {
		return array();
    }
	
	public function viewAction()
	{
		$student = $this->getDoctrine()
						->getRepository('AcmeCrudBundle:Student')
						->findAll();
		if(!$student){
			throw $this->createNotFoundException('There is no student');
		}				
		$build['student_item'] = $student;
		return $this->render('AcmeCrudBundle:Default:student_show_all.html.twig', $build);
	}
	
	public function showAction($id)
	{
		$student = $this->getDoctrine()
						->getRepository('AcmeCrudBundle:Student')
						->find($id);
		if(!$student){
			throw $this->createNotFoundException('No Student found by id ' . $id);
		}	
		$build['student_item'] = $student;			
		return $this->render('AcmeCrudBundle:Default:student_show.html.twig',$build);
	}
	
	public function addAction(Request $request) 
	{

		 $student = new Student();
		
		 $form = $this->createFormBuilder($student)
			->add('name', 'text')
			->add('roll', 'text')
			->add('age', 'text')
			->add('save', 'submit')
			->getForm();
	
		 $form->handleRequest($request);    
		 if ($form->isValid()) {
		   $em = $this->getDoctrine()->getManager();
		   $em->persist($student);
		   $em->flush();
		   return new Response('Student added successfuly');
		 }
		
		 $build['form'] = $form->createView();
		 return $this->render('AcmeCrudBundle:Default:student_add.html.twig', $build);
	 }
	 
	public function editAction($id, Request $request) 
	{
		$em = $this->getDoctrine()->getManager();
		$student = $em->getRepository('AcmeCrudBundle:Student')->find($id);
		if (!$student) {
		  throw $this->createNotFoundException(
				  'No Student found for id ' . $id
		  );
		}
		
		 $form = $this->createFormBuilder($student)
			->add('name', 'text')
			->add('roll', 'text')
			->add('age', 'text')
			->add('save', 'submit')
			->getForm();
	
		 $form->handleRequest($request);    
		 if ($form->isValid()) {
		   $em = $this->getDoctrine()->getManager();
		   //$em->persist($student);
		   $em->flush();
		   return new Response('Student updated successfuly');
		 }
		
		 $build['form'] = $form->createView();
		 return $this->render('AcmeCrudBundle:Default:student_add.html.twig', $build);
	 }
	 
	 public function deleteAction($id, Request $request) 
	 {
		$em = $this->getDoctrine()->getManager();
		$student = $em->getRepository('AcmeCrudBundle:Student')->find($id);
		if (!$student) {
		  throw $this->createNotFoundException(
				  'No Student found for id ' . $id
		  );
		}
	
		$form = $this->createFormBuilder($student)
				->add('delete', 'submit')
				->getForm();
	
		$form->handleRequest($request);
	
		if ($form->isValid()) {
		  $em->remove($student);
		  $em->flush();
		  return new Response('Student deleted successfully');
		}
		
		$build['form'] = $form->createView();
		return $this->render('AcmeCrudBundle:Default:student_add.html.twig', $build);
	}
	
	
}
